<!-- src/components/sidebars/main-sidebar.php -->
<section class="voice-ai-sidebar">
	<div id="post-body">
		<div class="postbox">
			<div class="inside">
				<h3 class="hndle">More from your Geeks & Creatives</h3>
				<div class="ad-section">
					<h3>Markey Digital Signage</h3>
					<a href="http://www.markeyds.com/?utm_source=wplogin&utm_medium=bannerAd&utm_campaign=MadeYouLook"
					   target="_blank">
						<img src="//wisdash.wpengine.com/wp-content/mu-plugins/wisnet-login/img/markey-tv.png">
						<h4>Grab attention with Markey Digital Signage.</h4>
					</a>
				</div>
				<hr>
				<div class="ad-section">
					<h3>Applicant Tracker</h3>
					<a href="https://www.wisnet.com/labs/#applicant-tracking"
					   target="_blank">
						<img src="//aqqky1uzlw43fojehzyf49hx-wpengine.netdna-ssl.com/wp-content/uploads/2015/12/ui-ApplicantTracker-fnl-multipleDevices.jpg">
						<h4>Track all your applicants in one place</h4>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>