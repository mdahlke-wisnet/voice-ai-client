<!-- src/components/pages/admin-index.php -->
<?php
global $Voice;
if (function_exists('get_admin_page_title')) {
	?>

	<div class="voice-ai-wrap">
		<section class="voice-ai-main">
			<header>
				<h1><?= esc_html(get_admin_page_title()); ?></h1>
			</header>
			<div id="post-body">
				<div class="postbox">
					<div class="inside">
						<h3 class="hndle">License Details</h3>
						<form action="options.php" method="post">
							<?php
							settings_fields('voice-ai');
							// output setting sections and their fields
							do_settings_sections('voice-ai');
							// output save settings button
							submit_button('Save Settings');
							?>
						</form>
					</div>
				</div>
			</div>
		</section>
		<?php get_plugin_part(VOICE_SIDEBARS . '/main-sidebar'); ?>
		<?php get_plugin_part(VOICE_ORGANISM . '/admin-page-footer'); ?>
	</div>
	<?php
}
?>