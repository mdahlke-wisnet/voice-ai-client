<!-- src/components/atoms/button.php -->
<?php
$default = array(
    'element' => 'div',
    'title' => 'Button',
    'href' => '#',
    'class' => 'page-title-action',
    'attr' => array(),
);
$receivedButton = receive('button', array());
$button = array_merge($default, $receivedButton);

$attrs = '';
foreach ($button['attr'] as $name => $value) {
    $attrs .= esc_attr($name) . '="' . esc_attr($value) . '"';
}
?>
<<?= $button['element']; ?> class="back-btn-wrap wrap">
<a class="<?= $button['class']; ?>" href="<?= $button['href']; ?>" <?= $attrs; ?>><?= $button['title']; ?></a>
</<?= $button['element']; ?>>
