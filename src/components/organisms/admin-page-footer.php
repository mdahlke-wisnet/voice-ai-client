<!-- src/components/organisms/admin-page-footer.php -->
<section class="admin-page-footer">
	<div class="logo-wrap">
		<span class="powered-by-text">Powered By:</span>
		<a href="//www.wisnet.com" target="_blank">
			<img class="img-center"
			     src="//aqqky1uzlw43fojehzyf49hx-wpengine.netdna-ssl.com/wp-content/themes/jump-off/img/logo.png">
		</a>
	</div>
</section>