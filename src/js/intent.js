/**
 * Created by michael on 2/26/17.
 */
(function ($) {

    $('body').on('click', '.js-update-intent', function () {
        var $phrases = $('.phrase');
        var userSays = [];

        $phrases.each(function () {
            var phrase = {
                id: $(this).attr('data-intent-id')
            };

            var fragments = [];
            var $fragments = $(this).find('.phrase-fragment');

            $fragments.each(function () {
                var fragment = {
                    text: $(this).text(),
                    alias: $(this).attr('data-alias'),
                    meta: $(this).attr('data-meta'),
                    'user-defined': $(this).attr('data-user-defined'),
                };
                fragments.push(fragment);
            });
            phrase['data'] = fragments;
            userSays.push(phrase);
        });

        var says = {
            id: $('#intent-id').val(),
            userSays: userSays
        };
        console.log(says);
        doIntentUpdate(says);

    });

    function doIntentUpdate(data) {
        $.ajax({
            type: 'post',
            url: ajax.url,
            data: {
                action: 'doIntentUpdate',
                data: data
            },
            success: function (e) {
                console.log(e);
            }
        })
    }

    $('.phrase').textWrap()
        .on('beforeWrap', function (e, elWrap, text) {
            // console.log(e);
        })
        .on('afterWrap', function (e, elWrap, text, range) {
            var $e = $(e.currentTarget);
            var $elWrap = $(elWrap);
            var vars = getMetaTypes();
            var $metaTypes = $('<div>', {
                'class': 'meta-types'
            });
            $('.meta-types').remove();

            $.each(vars, function (i, v) {
                $('<div>', {
                    'class': 'meta-type',
                    'data-value': v
                }).text(v).css({
                    padding: '3px 0',
                    whiteSpace: 'nowrap',
                    cursor: 'pointer'
                }).appendTo($metaTypes);
            });


            $('<div>', {
                'class': 'meta-type',
                'data-value': 'remove'
            }).text('Remove').css({
                padding: '3px 0',
                whiteSpace: 'nowrap',
                cursor: 'pointer'
            }).prependTo($metaTypes);

            $('<div>', {
                'class': 'meta-type',
                'data-value': 'cancel'
            }).text('Cancel').css({
                padding: '3px 0',
                whiteSpace: 'nowrap',
                cursor: 'pointer'
            }).prependTo($metaTypes);

            $('<div>', {
                'class': 'meta-selector-wrap'
            }).appendTo($metaTypes);

            $metaTypes.css({
                position: 'absolute',
                top: '100%',
                left: '50%',
                padding: '10px',
                background: 'white',
                border: '1px solid gray',
                transform: 'translateX(-50%)',
                zIndex: '999999999'
            });

            $elWrap.css({position: 'relative'}).prepend($metaTypes);

            $('.meta-type').on('click', function () {
                var value = $(this).attr('data-value');

                if (value === 'cancel') {
                    $metaTypes.remove();
                }
                else if (value === 'remove') {
                    $elWrap
                        .removeClass('has-meta')
                        .attr('data-meta', '@sys.ignore');
                    $metaTypes.remove();
                }
                else {
                    $elWrap
                        .addClass('phrase-fragment has-meta')
                        .attr('data-meta', $(this).attr('data-value'))
                        .attr('data-user-defined', 'true')
                        .attr('data-alias', '!@TODO');
                    $metaTypes.remove();
                }
            });
        });

    console.log(ajax);
    if(ajax.intent_action_name) {
        $('div[data-name="action_name"]').find('input').val(ajax.intent_action_name);
        $('input[name="post_title"]').val(ajax.intent_name);
    }

})(jQuery);

function getMetaTypes() {
    return [
        '@sys.given-name',
        '@sys.geo-city-us'
    ];
}