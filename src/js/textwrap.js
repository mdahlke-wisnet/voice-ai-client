;(function ($, window, document, undefined) {

    "use strict";

    var textWrap = "textWrap";
    var defaults = {
        elementWrap: 'span',
        classes: ''
    };

    function TextWrap(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = textWrap;
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(TextWrap.prototype, {
        init: function () {
            var self = this;
            $(this.element).mouseup(function () {
                var highlight = window.getSelection();
                self.wrapText(highlight);
            });
        },
        wrapText: function (text) {
            var didWrap;
            var $wrapEl = $('<' + this.settings.elementWrap + '/>', {
                'class': 'text-wrap ' + this.settings.classes
            });

            $(this.element).trigger('beforeWrap', $wrapEl, text);

            if (text.rangeCount) {
                var range = text.getRangeAt(0).cloneRange();
                if ((range.endOffset - range.startOffset) > 1) {
                    if (!$(text.baseNode.parentElement).hasClass('text-wrap')) {
                        didWrap = true;
                        range.surroundContents($wrapEl.get(0));
                        text.removeAllRanges();
                        text.addRange(range);
                    }
                    else {
                        didWrap = false;
                        $wrapEl = $(text.baseNode.parentElement);
                    }
                    $(this.element).trigger('afterWrap', $wrapEl, text, range, didWrap);
                }
                else {
                    $(this.element).trigger('didNotWrap');
                }
            }
        }
    });

    $.fn[textWrap] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + textWrap)) {
                $.data(this, "plugin_" +
                    textWrap, new TextWrap(this, options));
            }
        });
    };

})(jQuery, window, document);