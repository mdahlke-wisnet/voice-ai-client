/**
 * Created by michael on 3/9/17.
 */
Voice = {};
Voice.Client = {};
/**
 * Author: michael
 * Date: 3/10/17
 * Time: 8:58 AM
 * File: Flash
 */

(function ($) {
	
	$('body').on('click', '', function () {
		
	});
	
})(jQuery);
/**
 * Created by michael on 3/9/17.
 */
Voice.Client = typeof Voice.Client === 'undefined' ? {} : Voice.Client;


jQuery(document).ready(function ($) {

    console.log(Voice);

    Voice.Client = function () {

    };


    $.extend(Voice.Client, {
        init: function () {
            this.setupListeners();
        },
        setupListeners: function () {
            var self = this;
            $('body').on('submit', '#add-edit-client', function () {
                var data = $(this).serialize();

                console.log(data);

                self.add(data, function (status, data) {

                });

                return false;
            });

            $('.datepicker').on('click', function () {
                $(this).datepicker({
                    dateFormat: "yy-mm-dd"
                });
            });
        },
        add: function (client, callback) {

            $.ajax({
                type: 'post',
                url: ajax.url,
                data: {
                    action: 'add_edit_client',
                    data: client
                },
                success: function (response) {
                    console.log(response);
                    if (typeof callback === 'function') {
                        callback(response.success, response.data);
                    }
                }
            });
        }
    });

    Voice.Client.init();

});
;(function ($, window, document, undefined) {

    "use strict";

    var textWrap = "textWrap";
    var defaults = {
        elementWrap: 'span',
        classes: ''
    };

    function TextWrap(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = textWrap;
        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(TextWrap.prototype, {
        init: function () {
            var self = this;
            $(this.element).mouseup(function () {
                var highlight = window.getSelection();
                self.wrapText(highlight);
            });
        },
        wrapText: function (text) {
            var didWrap;
            var $wrapEl = $('<' + this.settings.elementWrap + '/>', {
                'class': 'text-wrap ' + this.settings.classes
            });

            $(this.element).trigger('beforeWrap', $wrapEl, text);

            if (text.rangeCount) {
                var range = text.getRangeAt(0).cloneRange();
                if ((range.endOffset - range.startOffset) > 1) {
                    if (!$(text.baseNode.parentElement).hasClass('text-wrap')) {
                        didWrap = true;
                        range.surroundContents($wrapEl.get(0));
                        text.removeAllRanges();
                        text.addRange(range);
                    }
                    else {
                        didWrap = false;
                        $wrapEl = $(text.baseNode.parentElement);
                    }
                    $(this.element).trigger('afterWrap', $wrapEl, text, range, didWrap);
                }
                else {
                    $(this.element).trigger('didNotWrap');
                }
            }
        }
    });

    $.fn[textWrap] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + textWrap)) {
                $.data(this, "plugin_" +
                    textWrap, new TextWrap(this, options));
            }
        });
    };

})(jQuery, window, document);
(function($){
    $('.table').on('click', '.tr', function(){
       $(this).toggleClass('open');
    });
})(jQuery);
/**
 * Created by michael on 2/26/17.
 */
(function ($) {

    $('body').on('click', '.js-update-intent', function () {
        var $phrases = $('.phrase');
        var userSays = [];

        $phrases.each(function () {
            var phrase = {
                id: $(this).attr('data-intent-id')
            };

            var fragments = [];
            var $fragments = $(this).find('.phrase-fragment');

            $fragments.each(function () {
                var fragment = {
                    text: $(this).text(),
                    alias: $(this).attr('data-alias'),
                    meta: $(this).attr('data-meta'),
                    'user-defined': $(this).attr('data-user-defined'),
                };
                fragments.push(fragment);
            });
            phrase['data'] = fragments;
            userSays.push(phrase);
        });

        var says = {
            id: $('#intent-id').val(),
            userSays: userSays
        };
        console.log(says);
        doIntentUpdate(says);

    });

    function doIntentUpdate(data) {
        $.ajax({
            type: 'post',
            url: ajax.url,
            data: {
                action: 'doIntentUpdate',
                data: data
            },
            success: function (e) {
                console.log(e);
            }
        })
    }

    $('.phrase').textWrap()
        .on('beforeWrap', function (e, elWrap, text) {
            // console.log(e);
        })
        .on('afterWrap', function (e, elWrap, text, range) {
            var $e = $(e.currentTarget);
            var $elWrap = $(elWrap);
            var vars = getMetaTypes();
            var $metaTypes = $('<div>', {
                'class': 'meta-types'
            });
            $('.meta-types').remove();

            $.each(vars, function (i, v) {
                $('<div>', {
                    'class': 'meta-type',
                    'data-value': v
                }).text(v).css({
                    padding: '3px 0',
                    whiteSpace: 'nowrap',
                    cursor: 'pointer'
                }).appendTo($metaTypes);
            });


            $('<div>', {
                'class': 'meta-type',
                'data-value': 'remove'
            }).text('Remove').css({
                padding: '3px 0',
                whiteSpace: 'nowrap',
                cursor: 'pointer'
            }).prependTo($metaTypes);

            $('<div>', {
                'class': 'meta-type',
                'data-value': 'cancel'
            }).text('Cancel').css({
                padding: '3px 0',
                whiteSpace: 'nowrap',
                cursor: 'pointer'
            }).prependTo($metaTypes);

            $('<div>', {
                'class': 'meta-selector-wrap'
            }).appendTo($metaTypes);

            $metaTypes.css({
                position: 'absolute',
                top: '100%',
                left: '50%',
                padding: '10px',
                background: 'white',
                border: '1px solid gray',
                transform: 'translateX(-50%)',
                zIndex: '999999999'
            });

            $elWrap.css({position: 'relative'}).prepend($metaTypes);

            $('.meta-type').on('click', function () {
                var value = $(this).attr('data-value');

                if (value === 'cancel') {
                    $metaTypes.remove();
                }
                else if (value === 'remove') {
                    $elWrap
                        .removeClass('has-meta')
                        .attr('data-meta', '@sys.ignore');
                    $metaTypes.remove();
                }
                else {
                    $elWrap
                        .addClass('phrase-fragment has-meta')
                        .attr('data-meta', $(this).attr('data-value'))
                        .attr('data-user-defined', 'true')
                        .attr('data-alias', '!@TODO');
                    $metaTypes.remove();
                }
            });
        });

    console.log(ajax);
    if(ajax.intent_action_name) {
        $('div[data-name="action_name"]').find('input').val(ajax.intent_action_name);
        $('input[name="post_title"]').val(ajax.intent_name);
    }

})(jQuery);

function getMetaTypes() {
    return [
        '@sys.given-name',
        '@sys.geo-city-us'
    ];
}