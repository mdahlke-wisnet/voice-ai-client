/**
 * Created by michael on 3/9/17.
 */
Voice.Client = typeof Voice.Client === 'undefined' ? {} : Voice.Client;


jQuery(document).ready(function ($) {

    console.log(Voice);

    Voice.Client = function () {

    };


    $.extend(Voice.Client, {
        init: function () {
            this.setupListeners();
        },
        setupListeners: function () {
            var self = this;
            $('body').on('submit', '#add-edit-client', function () {
                var data = $(this).serialize();

                console.log(data);

                self.add(data, function (status, data) {

                });

                return false;
            });

            $('.datepicker').on('click', function () {
                $(this).datepicker({
                    dateFormat: "yy-mm-dd"
                });
            });
        },
        add: function (client, callback) {

            $.ajax({
                type: 'post',
                url: ajax.url,
                data: {
                    action: 'add_edit_client',
                    data: client
                },
                success: function (response) {
                    console.log(response);
                    if (typeof callback === 'function') {
                        callback(response.success, response.data);
                    }
                }
            });
        }
    });

    Voice.Client.init();

});