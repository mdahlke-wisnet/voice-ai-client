<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 12/22/16
 * Time: 7:27 AM
 */
/**
 * Register meta box(es).
 */
function itinerary_register_meta_boxes() {
	add_meta_box(
		'intent-meta-box', __('Approval Status', VOICE_TEXT_DOMAIN), 'intent_approval', array(
		'intent',
	), "side", "core", null
	);
}

add_action('add_meta_boxes', 'itinerary_register_meta_boxes');

/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
function intent_approval($post, $metabox) {
	$Intent = new Ai_Controller_Intent();
	$approval = $Intent->approvalStatus($post->ID);
	?>
	<span class="label">Status: </span>
	<strong class="intent-status"><?= $approval['status']; ?></strong>
	<?php
	if ($approval['message']) {
		?>
		<br>
		<span class="label">Reason: </span>
		<strong class="intent-status"><?= $approval['message']; ?></strong>
		<?php
	}
}

/**
 * Save meta box content.
 *
 * @param int $post_id Post ID
 */
function itinerary_save_meta_box($post_id) {
	
	if (!isset($_POST["itinerary-meta-box-nonce"]) || !wp_verify_nonce($_POST["itinerary-meta-box-nonce"], basename(__FILE__))) {
		return $post_id;
	}
	
	if (!current_user_can("edit_post", $post_id)) {
		return $post_id;
	}
	
	if (defined("DOING_AUTOSAVE") && DOING_AUTOSAVE) {
		return $post_id;
	}
	
	$meta_box_checkbox_value = "";
	
	
	if (isset($_POST["allow-itinerary"])) {
		$meta_box_checkbox_value = 'true';
	}
	else {
		$meta_box_checkbox_value = 'false';
	}
	v($_POST["allow-itinerary"]);
	update_post_meta($post_id, "itinerary-allowed-on-page", $meta_box_checkbox_value);
	//	die();
}

add_action('save_post', 'itinerary_save_meta_box');