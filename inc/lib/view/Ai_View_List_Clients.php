<?php

/**
 * Client List Table
 *
 * User: michael
 * Date: 3/8/17
 * Time: 8:06 PM
 */
class Ai_View_List_Clients extends Ai_View_List_Table {

    function __construct() {
        global $status, $page;

        //Set parent defaults
        parent::__construct(array(
            'singular' => 'client',     //singular name of the listed records
            'plural' => 'clients',    //plural name of the listed records
            'ajax' => false        //does this table support ajax?
        ));

    }

    function column_default($item, $column_name) {
        return $item[$column_name];
    }

    function column_id($item) {
        $row_id = $item['id'];
        $actions = array(
            'edit' => sprintf('<a href="admin.php?page=wp_lic_mgr_addedit&edit_record=%s">Edit</a>', $row_id),
            'delete' => sprintf('<a href="admin.php?page=slm-main&action=delete_license&id=%s" onclick="return confirm(\'Are you sure you want to delete this record?\')">Delete</a>', $row_id),
        );
        return sprintf('%1$s <span style="color:silver"></span>%2$s',
            /*$1%s*/
            $item['id'],
            /*$2%s*/
            $this->row_actions($actions)
        );
    }


    function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/
            $this->_args['singular'],  //Let's simply re-purpose the table's singular label
            /*$2%s*/
            $item['id']                //The value of the checkbox should be the record's id
        );
    }

    function column_active($item) {
        if ($item['active'] == 1) {
            return 'active';
        }
        else {
            return 'inactive';
        }
    }


    function get_columns() {
        $columns = array(
            'cb' => '<input type="checkbox" />', //Render a checkbox
            'ClientName' => 'Client',
            'ClientID' => 'Client ID',
            'ClientKey' => 'License Key',
            'SecretKey' => 'Secret Key',
            'Domain' => 'Domains Allowed',
            'CreatedAt' => 'Created At',
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'ClientName' => array('ClientName', false),
            'ClientID' => array('ClientID', false),
            'CreatedAt' => array('CreatedAt', false),
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete' => 'Delete',
        );
        return $actions;
    }

    function process_bulk_action() {
        if ('delete' === $this->current_action()) {
            //Process delete bulk actions
            if (!isset($_REQUEST['item'])) {
                $error_msg = '<p>' . __('Error - Please select some records using the checkboxes', 'slm') . '</p>';
                echo '<div id="message" class="error fade">' . $error_msg . '</div>';
                return;
            }
            else {
                $nvp_key = $this->_args['singular'];
                $records_to_delete = $_GET[$nvp_key];
                foreach ($records_to_delete as $row) {
                    SLM_Utility::delete_license_key_by_row_id($row);
                }
                echo '<div id="message" class="updated fade"><p>Selected records deleted successfully!</p></div>';
            }
        }
    }


    /*
     * This function will delete the selected license key entries from the DB.
     */
    function delete_license_key($key_row_id) {
        SLM_Utility::delete_license_key_by_row_id($key_row_id);
        $success_msg = '<div id="message" class="updated"><p><strong>';
        $success_msg .= 'The selected entry was deleted successfully!';
        $success_msg .= '</strong></p></div>';
        echo $success_msg;
    }


    function prepare_items() {
        /**
         * First, lets decide how many records per page to show
         */
        $per_page = 20;
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->process_bulk_action();

        /* -- Ordering parameters -- */
        //Parameters that are going to be used to order the result
        $orderby = !empty($_GET["orderby"]) ? strip_tags($_GET["orderby"]) : 'ClientName';
        $order = !empty($_GET["order"]) ? strip_tags($_GET["order"]) : 'ASC';

        $ClientModel = new Ai_Model_Voice_Client();
        $ClientModel->setOrder($orderby . ' ' . $order);
        $ClientModel->setReturnType(ARRAY_A);

        if (isset($_GET['vui_client_search'])) {
            $search_term = trim(strip_tags($_GET['vui_client_search']));

            $data = array(
                'ClientID' => '%' . $search_term . '%',
                'ClientName' => '%' . $search_term . '%',
                'ClientKey' => '%' . $search_term . '%',
                'SecretKey' => '%' . $search_term . '%',
            );
            $ClientModel->addWhereGroup($data, 'LIKE', 'OR');
            $data = $ClientModel->query();
        }
        else {
            $data = $ClientModel->query();
        }

        $current_page = $this->get_pagenum();
        $total_items = count($data);
        $data = array_slice($data, (($current_page - 1) * $per_page), $per_page);
        $dataWithEdit = [];
        foreach ($data as $client) {
            $clientID = (int)$client['ClientID'];
            $client['ClientName'] = '<a href=' . Ai_Controller_Voice_Client::route('edit', $clientID) . ' title="Edit ' . $client['ClientName'] . '">' . $client['ClientName'] . '</a>';
            $dataWithEdit[] = $client;
        }

        $this->items = $dataWithEdit;
        $this->set_pagination_args(array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page' => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items / $per_page)   //WE have to calculate the total number of pages
        ));
    }
}