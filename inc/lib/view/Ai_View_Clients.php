<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 3/8/17
 * Time: 9:34 PM
 */
define('CLIENT_EDIT_KEY', 'add_edit');

class Ai_View_Clients {

    public function __construct() {
//        $client = filter_input(INPUT_GET, CLIENT_EDIT_KEY, FILTER_VALIDATE_INT);

        if (isset($_GET[CLIENT_EDIT_KEY])) {
            $this->client();
        }
        else {
            $this->index();
        }
    }

    public function loadPage($page) {
        $page = VOICE_PAGE . '/voice-clients-' . $page;
        get_plugin_part($page);
    }

    public function index() {
        $this->loadPage('index');
    }

    public function client() {
        $this->loadPage('client');
    }
}