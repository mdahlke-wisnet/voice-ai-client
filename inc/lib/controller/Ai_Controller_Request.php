<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 3/13/17
 * Time: 10:25 PM
 */
class Ai_Controller_Request {
	protected $headers;
	protected $httpHeaders;
	protected $id;
	private $resource;
	private $url;
	
	public function __construct() {
		$this->setUrl( 'https://www.secured-site7.com/wisnet-voice-ai/clients/wp-json/voiceai/v1/' );
//				$this->setUrl( 'http://www.wisnet-voice-ai.dev/wp-json/voiceai/v1/' );
		$this->setResource( 'info' );
		$this->addHeader( CURLOPT_RETURNTRANSFER, true );
		$this->id = null;
	}
	
	/**
	 * @return null
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * @param null $id
	 */
	public function setId( $id ) {
		$this->id = $id;
	}
	
	public function getResource() {
		return $this->resource;
	}
	
	/**
	 * @param string $resource
	 */
	public function setResource( $resource ) {
		$this->resource = $resource;
	}
	
	protected function getUrl() {
		return $this->url;
	}
	
	public function getCurlUrl() {
		return $this->getUrl() . $this->getResource() . '/' . $this->getId();
	}
	
	public function setUrl( $url ) {
		$this->url = $url;
	}
	
	protected function getHeaders() {
		return $this->headers;
	}
	
	protected function setHeaders( $headers ) {
		$this->headers = $headers;
	}
	
	public function addHeader( $key, $value ) {
		$headers = $this->getHeaders();
		
		$headers[ $key ] = $value;
		
		$this->setHeaders( $headers );
	}
	
	protected function getHTTPHeaders() {
		if ( !$this->httpHeaders ) {
			$this->httpHeaders = array();
		}
		
		return $this->httpHeaders;
	}
	
	protected function setHTTPHeaders( $headers ) {
		$this->httpHeaders = $headers;
	}
	
	public function addHTTPHeader( $key, $value ) {
		$httpHeaders = $this->getHTTPHeaders();
		
		$httpHeaders[] = $key . ': ' . $value;
		
		$this->setHTTPHeaders( $httpHeaders );
	}
	
	public function send() {
		$curl = curl_init();
		$this->addHeader( CURLOPT_URL, $this->getCurlUrl() );
		$this->addHeader( CURLOPT_HTTPHEADER, $this->getHTTPHeaders() );
		
		curl_setopt_array( $curl, $this->getHeaders() );
		$request = curl_exec( $curl );
		
		curl_close( $curl );
		
		return $request;
	}
}