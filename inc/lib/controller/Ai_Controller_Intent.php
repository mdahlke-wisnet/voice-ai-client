<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 3/10/17
 * Time: 3:25 PM
 */
class Ai_Controller_Intent {

    public function __construct() {
        // @TODO implement __construct()
    }

    public function __destruct() {
        // @TODO implement __destruct()

    }

    public function approvalStatus($intent) {

        $IntentRequest = new Ai_Controller_Request_Intent();
        $IntentRequest->addHTTPHeader('method', 'GET');
        $IntentRequest->addHTTPHeader('secretkey', get_option('voice_api_secret_key'));
        $IntentRequest->setId($intent);

        $request = $IntentRequest->send();

        try {
            $response = json_decode($request);

            if ($response === null) {
                return array('status' => 'Error connecting to wisnet.com, LLC', 'message' => false);
            }
            else if (isset($response->data->status)) {
                return array('status' => 'Error checking approval', 'message' => '');
            }

            return array('status' => $response->Status, 'message' => $response->Reason);
        } catch (Exception $e) {
            return array('status' => 'Error checking status.', 'message' => false);
        }

    }
}