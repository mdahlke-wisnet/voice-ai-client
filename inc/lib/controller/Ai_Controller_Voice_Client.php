<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 3/6/17
 * Time: 9:35 PM
 */
class Ai_Controller_Voice_Client extends Ai_Controller_Base {
	
	public function __construct( $ClientID = null ) {
		//		parent::__construct();
		if ( $ClientID ) {
			$this->setModel( new Ai_Model_Voice_Client( $ClientID ) );
		}
	}
	
	/**
	 * @return Ai_Model_Voice_Client
	 */
	public function model( $ClientID = null ) {
		if ( !$this->model ) {
			$client = new Ai_Model_Voice_Client( $ClientID );
			$this->setModel( $client );
		}
		
		return $this->model;
	}
	
	public function generateClientKey() {
		return 'ck_' . str_replace( '.', '', rand( 1000, 100000 ) . uniqid( '', true ) . rand( 0, 9 ) );
	}
	
	public function generateSecretKey() {
		return 'sk_' . str_replace( '.', '', rand( 0, 9 ) . uniqid( '', true ) . rand( 1000, 100000 ) );
	}
	
	public static function route( $action = 'view', $client = null ) {
		$route = '?page=voice-ai-clients';
		$query = '';
		if ( $action === 'add' || ( $action === 'edit' && is_int( $client ) ) ) {
			$query = '&add_edit=' . $client;
		}
		
		return $route . $query;
	}
	
	public function ajaxAddClient() {
		$data = array();
		
		parse_str( $_POST['data'], $data );
		
		$validRequest = wp_verify_nonce( $data['client_add_edit_nonce_val'], $data['client_add_edit_nonce_action'] );
		//        $uData = unserialize($data);
		
		if ( !$validRequest ) {
			//			Util::v( $data['client_add_edit_nonce_val'] );
			//            wp_send_json_error('Invalid Request');
		}
		//        Util::p($data);
		
		$Client = new Ai_Model_Voice_Client( $data['ClientID'] );
		$Client->setExpiresAt( $data['ExpiresAt'] );
		$Client->setCreatedAt( $data['CreatedAt'] );
		$Client->setClientKey( $data['ClientKey'] );
		//		Util::v( $data['ClientName'] );
		//		Util::v( $data['Status'] );
		//		Util::v( $data['CreatedAt'] );
		//		Util::v( $Client->getCreatedAt() );
		//		Util::v( $data['ActivatedAt'] );
		//		Util::v( $Client->getActivatedAt() );
		//		Util::v( $data['ExpiresAt'] );
		//		Util::v( $Client->getExpiresAt() );
		//		Util::v( '======================================================================================' );
		//		Util::v( '--------------------------------------------------------------------------------------' );
		$Client->setClientName( $data['ClientName'] );
		$Client->setDomain( $data['Domain'] );
		$Client->setStatus( $data['Status'] );
		$Client->setActivatedAt( $data['ActivatedAt'] );
		$Client->setSecretKey( $data['SecretKey'] );
		
		if ( $data['add-edit-client'] === 'add-client' ) {
			$insert = $Client->create();
		}
		else {
			$insert = $Client->update();
		}
		
		
		if ( $insert['status'] ) {
			wp_send_json_success(
				array(
					'message'   => 'client added',
					'insert_id' => Util::wpdb()->insert_id,
					'table'     => $Client->getTable(),
					'Client'    => $Client,
				)
			);
		}
		else {
			wp_send_json_error(
				array(
					'message' => $insert['message'],
					'errors'  => $insert['errors'],
				)
			);
		}
		
		wp_die();
	}
}