<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 12/21/16
 * Time: 10:04 PM
 */
class Ai_Controller_Voice_Settings {
	static private $instance;
	public $model;
	
	protected function __construct() {
		
	}
	
	/**
	 * @param null $id
	 *
	 * @return mixed
	 */
	public static function getInstance() {
		if (null === static::$instance) {
			self::$instance = new static();
			self::$instance->init();
			
			return self::$instance;
		}
		
		return static::$instance;
		
	}
	
	public function init() {
		add_action('admin_init', array($this, 'registerSettings'));
		
		$this->model = new Ai_Model_Voice_Settings();
	}
	
	public function load() {
		get_plugin_part(VOICE_PAGE . '/admin-index');
		
	}
	
	public function loadSettings() {
		get_plugin_part(VOICE_PAGE . '/voice-settings-index');
	}
	
	public function loadLog() {
		get_plugin_part(VOICE_PAGE . '/voice-log-index');
	}
	
	public function loadClients() {
		new Ai_View_Clients();
	}
	
	public function registerSettings() {
		register_setting('voice-ai', 'voice_api_client_key');
		register_setting('voice-ai', 'voice_api_secret_key');
		
		add_settings_section('voice_api_keys', '', array($this, 'voice_api_keys_section'), 'voice-ai');
		add_settings_field(
			'voice_api_client_key', 'Client Key', array($this, 'voice_api_keys_field_client_key'), 'voice-ai', 'voice_api_keys'
		);
		add_settings_field(
			'voice_api_secret_key', 'Secret Key', array($this, 'voice_api_keys_field_secret_key'), 'voice-ai', 'voice_api_keys'
		);
	}
	
	public function addAdminMenu() {
//		add_menu_page(
//			__('Voice AI', 'voice-ai'), 'Voice AI', 'manage_options', 'voice-ai', array($this, 'load'), 'dashicons-screenoptions', 2
//		);
		add_submenu_page(
			'edit.php?post_type=intent', 'Voice Settings', 'Voice Settings', 'manage_options', 'voice-ai-settings', array($this, 'load'), null
		);
//		add_submenu_page(
//			'intent', 'Voice Settings', 'New Intent', 'manage_options', 'post-new.php?post_type=intent', null
//		);
		/*
		add_submenu_page(
			'voice-ai', 'Voice Log', 'Voice Log', 'manage_options', 'voice-ai-log', array($this, 'loadLog')
		);
		*/
	}
	
	public function voice_api_keys_section() {
		?>
		
		<?php
	}
	
	public function voice_api_keys_field_client_key() {
		// get the value of the setting we've registered with register_setting()
		$setting = get_option('voice_api_client_key');
		// output the field
		?>
		<input type="text" name="voice_api_client_key" value="<?= isset($setting) ? esc_attr($setting) : ''; ?>">
		<?php
	}
	
	public function voice_api_keys_field_secret_key() {
		
		// get the value of the setting we've registered with register_setting()
		$setting = get_option('voice_api_secret_key');
		// output the field
		?>
		<input type="text" name="voice_api_secret_key" value="<?= isset($setting) ? esc_attr($setting) : ''; ?>">
		<?php
	}
}