<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 3/6/17
 * Time: 9:35 PM
 */
class Ai_Controller_REST_Voice_Client_Intent {
	
	//	public function __construct( $ClientID = null ) {
	//		$this->setModel( new Ai_Model_Voice_Client( $ClientID ) );
	//	}/ Here initialize our namespace and resource name.
	public function __construct() {
		$this->namespace = '/voiceaiclient/v1';
		$this->resource_name = 'intent';
	}
	
	// Register our routes.
	public function register_routes() {
		register_rest_route(
			$this->namespace, '/' . $this->resource_name . '/(?P<id>[\d]+)', array(
				                array(
					                'methods'             => 'GET',
					                'callback'            => array( $this, 'get' ),
					                'permission_callback' => array( $this, 'getClientPermissionCheck' ),
				                ),
			                )
		);
	}
	
	public function sendResponse( $response ) {
		wp_send_json( $response );
	}
	
	public function get( WP_REST_Request $request ) {
		$post = get_post( $request->get_param( 'id' ) );
		$fields = get_fields( $post->ID );
		
		$response = array(
			'post'   => $post,
			'fields' => $fields,
		);
		$this->sendResponse( $response );
	}
	
	public function getClientPermissionCheck( $request ) {
		if ( strpos( $request->get_header( 'ClientDomain' ), 'wisnet-voice-ai.dev' ) === false ) {
			return new WP_Error( 'rest_forbidden', esc_html__( 'You cannot view the post resource.' ), array( 'status' => $this->authorization_status_code() ) );
		}
		
		return true;
	}
	
	public function authorization_status_code() {
		
		$status = 401;
		
		if ( is_user_logged_in() ) {
			$status = 403;
		}
		
		return $status;
	}
}
