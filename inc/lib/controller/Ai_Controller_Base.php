<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 12/7/16
 * Time: 8:17 PM
 */
class Ai_Controller_Base {
    private static $routeNames = array();
    protected $model;

    /**
     * @param      $action
     * @param      $callback
     * @param null $class
     */
    public function ajax_action($action, $callback, $class = null) {
        if (!$class) {
            $class = $this;
        }

        if (method_exists($class, $callback)) {
            add_action('wp_ajax_' . $action, array($class, $callback));
            add_action('wp_ajax_nopriv_' . $action, array($class, $callback));
        }
        else {
            add_action('wp_ajax_' . $action, array($class, 'invalid_method'));
            add_action('wp_ajax_nopriv_' . $action, array($class, 'invalid_method'));
        }
    }

    /**
     * @param $callback
     */
    public function invalid_method($callback) {
        wp_send_json(array('status' => false, 'message' => 'Invalid method call for action: ' . $_REQUEST['action']));

        wp_die();
    }

    /**
     * @param $var
     */
    protected function debug($var) {
        if (isset($_GET['debug'])) {
            p($var);
        }
    }

    public static function route($args = array('agent' => null, 'object' => null, 'intent' => null)) {
        $query = '?page=voice-ai&';
        foreach ($args as $key => $value) {
            if ($value) {
                $query .= $key . '=' . $value . '&';
            }
        }

        return $query;

    }

    /**
     * Specify the value
     * %GET:$_GET['var']% is a placeholder to grab a value from the URL
     *
     * @param $name
     */
    public static function addNamedRoute($name, $value) {
        self::$routeNames[$name] = $value;
    }

    /**
     * All named routes will be in the context of the voice ai plugin
     * Which means they will always begin with ?page=voice-ai
     * Here, we are really looking for $_GET vars to add to the string.
     *
     * @param $name
     *
     * @return mixed
     */
    public static function namedRoute($name) {
        $query = '?page=voice-ai&';
        $args = self::$routeNames[$name];

        foreach ($args as $key => $val) {
            $getKey = str_replace('%GET:', '', $val);

            $getVar = isset($_GET[$getKey]) ? $_GET[$getKey] : false;

            if ($getVar !== false) {
                $query .= $key . '=' . $getVar . '&';
            }
        }

        return $query;
    }

    protected function setModel($model) {
        $this->model = $model;
    }

    public function model() {
        return $this->model;
    }
}