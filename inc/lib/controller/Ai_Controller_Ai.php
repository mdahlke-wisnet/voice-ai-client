<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 2/22/17
 * Time: 9:13 AM
 */
class Ai_Controller_Ai {
	static protected $instance;
	protected $parameters;
	private $request;
	private $response;
	private $service;
	private $chatBotServices;
	
	protected function __construct() {
		
	}
	
	/**
	 * @return Ai_Controller_Ai
	 */
	public static function getInstance() {
		if ( null === static::$instance ) {
			self::$instance = new static();
			
			return self::$instance;
		}
		
		return static::$instance;
		
	}
	
	/**
	 * @return array
	 */
	public function getChatBotServices() {
		if ( empty( $this->chatBotServices ) ) {
			$this->chatBotServices = array();
		}
		
		return $this->chatBotServices;
	}
	
	/**
	 * @param array $chatBotServices
	 */
	public function setChatBotServices( $chatBotServices ) {
		$this->chatBotServices = $chatBotServices;
	}
	
	/**
	 * @param string|array $chatBotService
	 */
	public function addChatBotService( $chatBotService ) {
		if ( !is_array( $chatBotService ) ) {
			$chatBotService = array( $chatBotService );
		}
		$this->setChatBotServices( array_merge( $this->getChatBotServices(), $chatBotService ) );
		
		return $this;
	}
	
	public function init( $request ) {
		$this->addChatBotService( 'facebook' )->addChatBotService( 'twitter' );
	}
	
	public function __destruct() {
		
	}
	
	/**
	 * @param string $service
	 *
	 * @return bool
	 */
	public function isChatbot( $service = '' ) {
		return in_array( $service, $this->getChatBotServices() );
	}
	
	/**
	 * @return Ai_Controller_Request
	 */
	public function request() {
		return $this->request;
	}
	
	/**
	 * @return Ai_Controller_Service
	 */
	public function service() {
		return $this->service;
	}
	
	/**
	 * @return Ai_Controller_Response
	 */
	public function response() {
		return $this->response;
	}
	
	public static function activationHook() {
		$Ai = new Ai_Controller_Ai();
		$Ai->insertPosts();
		
		if ( version_compare( get_option( 'voice_plugin_version' ), VOICE_PLUGIN_VERSION, '<' ) ) {
			$Ai->runDatabaseUpdates();
			$Ai->insertNewIntents( VOICE_PLUGIN_VERSION );
		}
		
		update_option( 'voice_plugin_version', VOICE_PLUGIN_VERSION );
	}
	
	public function runDatabaseUpdates() {
		
		$sql = false;
		
		if ( $sql ) {
			
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );
		}
		
		if ( version_compare( '1.3.1', VOICE_PLUGIN_VERSION, '=' ) ) {
			$ghu = Fragen\GitHub_Updater\Base::getOptions();
			
			$ghu['bitbucket_username'] = 'wisnet-bitbucket';
			$ghu['bitbucket_password'] = base64_decode( 'VnNIZ050QnF3Z2U1V3Q5ekgyTHg=' );
			
			$ghuBase = new Fragen\GitHub_Updater\Base();
			Fragen\GitHub_Updater\Base::setOptions( $ghu );
			$ghuBase->set_options_filter();
		}
	}
	
	private function insertPosts() {
		$intents = array(
			'hours'           => 'Hours',
			'location'        => 'Location',
			'what_do_you_do'  => 'What do you do',
			'who_works_there' => 'Who Works There',
		);
		
		$this->insertIntent( $intents );
	}
	
	private function insertNewIntents( $version = null ) {
		if ( !$version ) {
			return;
		}
		
		$intents = [];
		
		if ( version_compare( $version, '1.2.1', '>=' ) ) {
			$intents['event'] = 'Events';
		}
		
		if ( version_compare( $version, '1.3.2', '>=' ) ) {
			$intents['what_does_x_do'] = 'What does X do';
		}
		
		if ( $intents ) {
			$this->insertIntent( $intents, 'new_intent' );
		}
	}
	
	/**
	 * @param array  $intents Associative array of intent action names as the key and the post title as the value
	 *                        EXAMPLE: array( 'who_works_there' => 'Who Works There');
	 * @param string $status  Post status, defaults to draft
	 */
	private function insertIntent( $intents = array(), $status = 'draft' ) {
		foreach ( $intents as $key => $val ) {
			$args = array(
				'post_type'   => 'intent',
				'post_status' => 'any',
				'post_title'  => $val,
				'meta_key'    => 'action_name',
				'meta_value'  => $key,
			);
			
			
			$post = new WP_Query( $args );
			
			
			if ( !$post->have_posts() ) {
				$postarr = array(
					'post_type'    => 'intent',
					'post_content' => '',
					'post_status'  => $status,
					'post_title'   => $val,
					'meta_input'   => array(
						'action_name' => $key,
					),
				);
				
				wp_insert_post( $postarr );
			}
		}
	}
}