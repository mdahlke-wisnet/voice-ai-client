<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 3/13/17
 * Time: 10:25 PM
 */
class Ai_Controller_Request_Intent extends Ai_Controller_Request {

    public function __construct() {
        parent::__construct();
        $this->setResource('intent');
    }

}