<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 3/6/17
 * Time: 9:35 PM
 */
class Ai_Controller_REST_Voice_Client {
	
	//	public function __construct( $ClientID = null ) {
	//		$this->setModel( new Ai_Model_Voice_Client( $ClientID ) );
	//	}/ Here initialize our namespace and resource name.
	public function __construct() {
		$this->namespace = '/voice-ai-client/v1';
		$this->resource_name = 'client';
	}
	
	// Register our routes.
	public function register_routes() {
		register_rest_route(
			$this->namespace, '/' . $this->resource_name . '/(?P<id>[\d]+)', array(
				                array(
					                'methods'             => 'GET',
					                'callback'            => array( $this, 'get' ),
					                'permission_callback' => array( $this, 'getClientPermissionCheck' ),
				                ),
			                )
		);
	}
	
	public function sendResponse( $response ) {
		//		new WP_REST_Response( $response );
		wp_send_json( $response );
	}
	
	public function get( WP_REST_Request $request ) {
		$headers = $_SERVER['Authorization'];
		$ClientModel = new Ai_Model_Voice_Client( $request->get_param( 'id' ) );
		
		$response = array(
			'ClientID'   => $ClientModel->getClientID(),
			'ClientName' => $ClientModel->getClientName(),
		);
		$this->sendResponse( $response );
	}
	
	public function getClientPermissionCheck( $request ) {
		$ClientModel = new Ai_Model_Voice_Client( $request->get_param( 'id' ) );
		
		if ( $ClientModel->getSecretKey() == $request->get_header( 'secretkey' ) ) {
			return true;
		}
		
		return new WP_Error( 'rest_forbidden', esc_html__( 'You cannot view the post resource.' ), array( 'status' => $this->authorization_status_code() ) );
		
	}
	
	public function authorization_status_code() {
		
		$status = 401;
		
		if ( is_user_logged_in() ) {
			$status = 403;
		}
		
		return $status;
	}
}
