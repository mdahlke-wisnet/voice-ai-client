<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 11/22/16
 * Time: 9:23 PM
 */
class Ai_Controller_Voice extends Ai_Controller_Base {
	
	/**
	 * Singleton instance
	 *
	 * @since v1.0.0
	 * @access private
	 * @var class
	 */
	static private $instance;
	
	/**
	 *
	 *
	 * @since v1.0.0
	 * @access public
	 * @var
	 */
	public $model;
	
	/**
	 *
	 *
	 * @since v1.0.0
	 * @access public
	 * @var bool
	 */
	public $isLoaded = false;
	
	/**
	 *
	 *
	 * @since v1.0.0
	 * @access public
	 * @var bool
	 */
	public $isModelLoaded = false;
	
	/**
	 * We don't use the constructor since we are using the singleton pattern
	 *      http://www.phptherightway.com/pages/Design-Patterns.html#singleton
	 *
	 * @since v1.0.0
	 * @acess protected
	 * Voice constructor.
	 */
	protected function __construct() {
	}
	
	/**
	 * Prevent cloning
	 *
	 * @since v1.0.0
	 * @access private
	 */
	private function __clone() {
	}
	
	/**
	 * Prevent serialization
	 *
	 * @since v1.0.0
	 * @access private
	 */
	private function __wakeup() {
	}
	
	/**
	 * Get the Voice
	 *
	 * @since v1.0.0
	 *
	 * @param int $id
	 *
	 * @return mixed
	 */
	public static function getInstance($id = null) {
		if (null === static::$instance || $id) {
			self::$instance = new static();
			self::$instance->init();
			
			if ($id) {
				self::$instance->load($id);
			}
			
			return self::$instance;
		}
		
		return static::$instance;
		
	}
	
	/**
	 * Run the install functions on plugin activation
	 *
	 * @todo Create install class to handle this
	 *
	 * @since v1.0.0
	 *
	 */
	public static function install() {
		Ai_Model_Voice::install();
	}
	
	/**
	 *
	 * @since v1.0.0
	 *
	 * @param $id
	 */
	private function init() {
		
		$this->isLoaded = true;
	}
}