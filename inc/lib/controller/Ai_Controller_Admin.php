<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 1/5/17
 * Time: 8:12 PM
 */
class Ai_Controller_Admin {
	
	static private $instance;
	public $model;
	
	protected function __construct() {
		
	}
	
	/**
	 * @param null $id
	 *
	 * @return mixed
	 */
	public static function getInstance() {
		if (null === static::$instance) {
			self::$instance = new static();
			self::$instance->init();
			
			return self::$instance;
		}
		
		return static::$instance;
		
	}
	
	public function init() {
		add_action('admin_menu', array($this, 'add_menus'));
	}
	
	public function load() {
		get_plugin_part(VOICE_PAGE . '/admin-index');
	}
	
	public function add_menus() {
		global $VoiceSettings;
		
		$VoiceSettings->addAdminMenu();
	}
}