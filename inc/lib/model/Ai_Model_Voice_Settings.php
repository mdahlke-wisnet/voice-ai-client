<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 12/22/16
 * Time: 9:33 PM
 */
class Ai_Model_Voice_Settings extends Ai_Model_Base {
	
	private $showPosition;
	private $autoDisplay;
	
	/**
	 * @return mixed
	 */
	public function getShowPosition() {
		return $this->showPosition;
	}
	
	/**
	 * @param mixed $showPosition
	 */
	public function setShowPosition($showPosition) {
		$this->showPosition = $showPosition;
	}
	
	public function getShowPositionOptions() {
		return array(
			'before-content' => 'Before Content',
			'after-content' => 'After Content',
			'shortcode' => 'Shortcode Only',
		);
	}
	
	/**
	 * @return mixed
	 */
	public function getAutoDisplay() {
		return $this->autoDisplay;
	}
	
	/**
	 * @param mixed $autoDisplay
	 */
	public function setAutoDisplay($autoDisplay) {
		$this->autoDisplay = $autoDisplay;
	}
	
	public function getAutoDisplayOptions() {
		return array(
			'auto' => 'On page load (Auto)',
			'shortcode' => 'Shortcode Only (or I\'ll create a link)',
		);
	}
	
	
	public function __construct() {
		$this->init();
	}
	
	public function init() {
//		$this->setShowPosition(get_option('show_position'));
//		$this->setAutoDisplay(get_option('itinerary_garage_auto_display'));
	}
}