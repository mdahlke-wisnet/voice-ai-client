<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 3/6/17
 * Time: 9:13 PM
 */
class Ai_Model_Voice_Client extends Ai_Model_Base {
	private $ClientID;
	private $ClientName;
	private $Domain;
	private $ClientKey;
	private $SecretKey;
	private $ActivatedAt;
	private $ExpiresAt;
	private $CreatedAt;
	private $Status;
	
	/**
	 * @return mixed
	 */
	public function getClientID() {
		return $this->ClientID;
	}
	
	/**
	 * @param mixed $ClientID
	 */
	public function setClientID( $ClientID ) {
		$this->ClientID = $ClientID;
	}
	
	/**
	 * @return mixed
	 */
	public function getClientName() {
		return $this->ClientName;
	}
	
	/**
	 * @param mixed $ClientName
	 */
	public function setClientName( $ClientName ) {
		$this->ClientName = $ClientName;
	}
	
	/**
	 * @return mixed
	 */
	public function getDomain() {
		return $this->Domain;
	}
	
	/**
	 * @param mixed $Domain
	 */
	public function setDomain( $Domain ) {
		$this->Domain = $Domain;
	}
	
	/**
	 * @return mixed
	 */
	public function getClientKey( $generateKeyOnEmpty = true ) {
		if ( empty( $this->ClientKey ) && $generateKeyOnEmpty ) {
			$ClientController = new Ai_Controller_Voice_Client();
			$key = $ClientController->generateClientKey();
			
			$this->setClientKey( $key );
		}
		
		return $this->ClientKey;
	}
	
	/**
	 * @param mixed $ClientKey
	 */
	public function setClientKey( $ClientKey ) {
		$this->ClientKey = $ClientKey;
	}
	
	/**
	 * @return mixed
	 */
	public function getSecretKey( $generateKeyOnEmpty = true ) {
		if ( empty( $this->SecretKey ) && $generateKeyOnEmpty ) {
			$ClientController = new Ai_Controller_Voice_Client();
			$key = $ClientController->generateSecretKey();
			
			$this->setSecretKey( $key );
		}
		
		return $this->SecretKey;
	}
	
	/**
	 * @param mixed $SecretKey
	 */
	public function setSecretKey( $SecretKey ) {
		$this->SecretKey = $SecretKey;
	}
	
	/**
	 * @return mixed
	 */
	public function getCreatedAt( $pretty = false ) {
		$format = 'Y-m-d H:i:s';
		if ( $pretty ) {
			$format = 'M d, Y';
		}
		
		return date( $format, strtotime( $this->CreatedAt ) );
	}
	
	/**
	 * @param mixed $CreatedAt
	 */
	public function setCreatedAt( $CreatedAt ) {
		$this->CreatedAt = ( $CreatedAt );
	}
	
	/**
	 * @return mixed
	 */
	public function getActivatedAt() {
		if ( !$this->ActivatedAt ) {
			$this->setActivatedAt( date( 'Y-m-d' ) );
		}
		
		return date( 'Y-m-d', strtotime( $this->ActivatedAt ) );
	}
	
	/**
	 * @param mixed $ActivatedAt
	 */
	public function setActivatedAt( $ActivatedAt ) {
		$this->ActivatedAt = $this->formatDate( $ActivatedAt );
	}
	
	/**
	 * @return mixed
	 */
	public function getExpiresAt() {
		if ( !$this->ExpiresAt ) {
			$date = $this->formatDate();
			$this->setExpiresAt( $date );
		}
		
		return $this->ExpiresAt;
	}
	
	/**
	 * @param mixed $ExpiresAt
	 */
	public function setExpiresAt( $ExpiresAt ) {
		$this->ExpiresAt = $this->formatDate( $ExpiresAt );
	}
	
	public function formatDate( $date = null, $addDays = null ) {
		if ( !$date ) {
			$date = date( 'Y-m-d' );
		}
		
		if ( is_int( $date ) ) {
			$date = date( 'Y-m-d', $date );
		}
		//        $date = date('Y-m-d', strtotime(str_replace('-', '/', $date)));
		
		if ( $addDays ) {
			//            $date = date('Y-m-d H:i:s');
		}
		
		return $date;
	}
	
	/**
	 * @return mixed
	 */
	public function getStatus() {
		return (int) $this->Status;
	}
	
	/**
	 * @param mixed $Status
	 */
	public function setStatus( $Status ) {
		$this->Status = (int) $Status;
	}
	
	public function getClientStatuses() {
		$statuses = array(
			1 => 'Active',
			2 => 'Pending',
			3 => 'Inactive',
			4 => 'Suspended',
			5 => 'Awaiting Payment',
		);
		
		return $statuses;
		
	}
	
	public function getClientStatusesFormatted( $active = 2, $before = '', $after = '' ) {
		$statuses = $this->getClientStatuses();
		
		$formatted = [];
		foreach ( $statuses as $value => $label ) {
			$formatted[ $value ] = str_replace( '%id%', $value, $before ) . $label . str_replace( '%id%', $value, $after );
		}
		
		return implode( "\n", $formatted );
	}
	
	public function __construct( $ClientID = null ) {
		parent::__construct();
		
		$this->table = Util::wpdb()->prefix . 'voice_clients';
		
		if ( $ClientID ) {
			$Client = $this->fetch( $ClientID );
			
			$this->mapper( $Client );
		}
		
	}
	
	/**
	 * @param $ClientID
	 *
	 * @return Ai_Model_Voice_Client|bool
	 */
	public function fetch( $ClientID ) {
		$sql = '
			SELECT *
			FROM ' . $this->table . '
			WHERE ClientID = %d
		';
		$data = array( $ClientID );
		$results = $this->get_row( $sql, $data );
		
		if ( !is_wp_error( $results ) ) {
			return $results;
		}
		
		return false;
	}
	
	/**
	 * @param $ClientID
	 *
	 * @return Ai_Model_Voice_Client
	 */
	public function get( $ClientID ) {
		$client = $this->fetch( $ClientID );
		
		return new Ai_Model_Voice_Client( $client );
	}
	
	/**
	 * @return array
	 */
	public function fetchAll() {
		
		$sql = '
			SELECT *
			FROM ' . $this->table . '
		';
		
		$results = $this->get_results( $sql );
		
		if ( !is_wp_error( $results ) ) {
			$clients = array();
			foreach ( $results as $client ) {
				$clients[] = new Ai_Controller_Voice_Client( $client->ClientID );
			}
			
			return $clients;
		}
	}
	
	public function create( Ai_Model_Voice_Client $Client = null ) {
		if ( !$Client ) {
			$Client = $this;
		}
		
		$errors = array();
		$valid = true;
		
		if ( empty( $Client->getClientName() ) ) {
			$valid = false;
			$errors[] = 'You must provide a client name.';
		}
		
		if ( !$valid ) {
			return array( 'status' => false, 'message' => 'Invalid data', 'errors' => $errors );
		}
		
		$data = array(
			'ClientName'  => $Client->getClientName(),
			'Domain'      => $Client->getDomain(),
			'ClientKey'   => $Client->getClientKey( true ),
			'SecretKey'   => $Client->getSecretKey( true ),
			'Status'      => $Client->getStatus(),
			'ExpiresAt'   => $Client->getExpiresAt(),
			'CreatedAt'   => $Client->getCreatedAt(),
			'ActivatedAt' => $Client->getActivatedAt(),
		);
		
		$format = array(
			$this->placeholder->string,
			$this->placeholder->string,
			$this->placeholder->string,
			$this->placeholder->string,
			$this->placeholder->string,
			$this->placeholder->string,
			$this->placeholder->string,
		);
		
		$inserted = $this->insert( $data, $format );
		
		if ( $inserted ) {
			return array( 'status' => true, 'message' => 'Success' );
		}
		
		return array( 'status' => false, 'message' => 'Could not insert.', 'errors' => [ Util::wpdb()->last_error ] );
		
	}
	
	public function update( Ai_Model_Voice_Client $Client = null ) {
		if ( !$Client ) {
			$Client = $this;
		}
		
		$errors = array();
		$valid = true;
		
		if ( empty( $Client->getClientName() ) ) {
			$valid = false;
			$errors[] = 'You must provide a client name.';
		}
		
		if ( !$valid ) {
			return array( 'status' => false, 'message' => 'Invalid data', 'errors' => $errors );
		}
		
		$data = array(
			'ClientName'  => $Client->getClientName(),
			'Domain'      => $Client->getDomain(),
			'ClientKey'   => $Client->getClientKey(),
			'SecretKey'   => $Client->getSecretKey(),
			'Status'      => $Client->getStatus(),
			'ExpiresAt'   => $Client->getExpiresAt(),
			'ActivatedAt' => $Client->getActivatedAt(),
		);
		
		$where = array(
			'ClientID' => $Client->getClientID(),
		);
		
		$format = array(
			$this->placeholder->string,
			$this->placeholder->string,
			$this->placeholder->string,
			$this->placeholder->string,
			$this->placeholder->string,
			$this->placeholder->string,
		);
		
		$whereFormat = array(
			$this->placeholder->digit,
		);
		
		$inserted = parent::update( $data, $where, $format, $whereFormat );
		
		if ( $inserted ) {
			return array( 'status' => true, 'message' => 'Success' );
		}
		
		return array( 'status' => false, 'message' => 'Could not update.', 'errors' => [ Util::wpdb() ] );
		
	}
}