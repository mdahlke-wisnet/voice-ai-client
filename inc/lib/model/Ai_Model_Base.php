<?php

/**
 * Created by PhpStorm.
 * User: michael
 * Date: 12/7/16
 * Time: 7:32 PM
 */
class Ai_Model_Base {
    protected $table;
    protected $primaryKeyColumn;
    protected $UserID;
    protected $placeholder;
    protected $select;
    protected $from;
    protected $join = '';
    protected $where = '';
    protected $order = '';
    protected $having = '';
    protected $returnType;

    final public function getTable() {
        return $this->getTable;
    }

    /**
     * @return mixed
     */
    public function getSelect() {
        if (empty($this->select)) {
            $this->setSelect('*');
        }
        return $this->select;
    }

    /**
     * @param mixed $select
     */
    public function setSelect($select) {
        $this->select = $select;
    }

    /**
     * @return mixed
     */
    public function getFrom() {
        if (empty($this->from)) {
            $this->setFrom($this->table);
        }
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from) {
        $this->from = $from;
    }

    /**
     * @return mixed
     */
    public function getJoin() {
        return $this->join;
    }

    /**
     * @param mixed $join
     */
    public function setJoin($join) {
        $this->join = $join;
    }

    /**
     * @return mixed
     */
    public function getWhere() {
        if (is_array($this->where)) {
            return implode(' AND ', $this->where);
        }
        return $this->where;
    }

    /**
     * @param array $where
     */
    public function setWhere($where) {
        $this->where = $where;
    }

    public function addWhere($where, $operator = '=', $relation = 'AND') {
        if (!is_array($this->where)) {
            $this->where = [];
        }

        $this->where[] = $where;
    }

    public function addWhereGroup($data, $operator = '=', $relation = 'AND') {
        $wheres = [];
        foreach ($data as $key => $val) {
            $wheres[] = ' ' . $key . ' ' . $operator . ' \'' . $val . '\' ';
        }

        $where = implode($relation, $wheres);
        $this->addWhere($where);
    }

    /**
     * @return mixed
     */
    public function getOrder() {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order) {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getHaving() {
        return $this->having;
    }

    /**
     * @param mixed $having
     */
    public function setHaving($having) {
        $this->having = $having;
    }

    /**
     * @return mixed
     */
    public function getReturnType() {
        if (empty($this->returnType)) {
            $this->returnType = OBJECT;
        }
        return $this->returnType;
    }

    /**
     * @param mixed $returnType
     */
    public function setReturnType($returnType) {
        $this->returnType = $returnType;
    }


    public function __construct() {
        $this->UserID = get_current_user_id();
        $this->placeholder = (object)array(
            'digit' => '%d',
            'string' => '%s',
            'float' => '%f',
        );
    }

    protected function debug($var) {
        if (isset($_GET['debug'])) {
            Util::p($var);
        }
    }

    protected function mapper($data = null, $class = null) {
        if (!$class) {
            $class = $this;
        }
        if (!is_array($data) && !is_object($data) && gettype($data) === 'integer') {
            $this->quick_load($data);
        }
        if ($data) {
            foreach ($data as $column => $value) {
                $column = Util::camelCase($column);
                if (method_exists($class, 'set' . $column)) {
                    call_user_func(array($class, 'set' . $column), $value);
                }
            }
        }

        return $this;
    }

    protected function quick_load($id = null) {
        if (!$id || !isset($this->table) || !isset($this->primaryKeyColumn)) {
            return false;
        }
        $sql = '
				SELECT *
				FROM ' . $this->table . '
				WHERE ' . $this->primaryKeyColumn . ' = ' . $this->placeholder->digit . '
			';
        $data = array($id);

        //		Util::p( $sql, $data );
        return $this->prepare($sql, $data);
    }

    protected function prepare($sql, $data) {
        $returnSql = Util::wpdb()->prepare($sql, $data);
        if ($returnSql) {
            return $returnSql;
        }

        return array(
            'error' => Util::wpdb()->last_error,
            'sql' => $sql,
            'data' => $data,
        );
    }

    protected function get_row($sql, $data = null) {
        if ($data) {
            $sql = $this->prepare($sql, $data);
        }

        $results = Util::wpdb()->get_row($sql, $this->getReturnType());
        if ($results) {
            return $results;
        }

        return new WP_Error('1002333', 'Error fetching results', $data);
//		return array(
//			'error' => $results->last_error,
//			'sql' => $sql,
//			'data' => $data,
//		);
    }

    protected function get_results($sql, $data = null) {
        if ($data) {
            $sql = $this->prepare($sql, $data);
        }

        $results = Util::wpdb()->get_results($sql, $this->getReturnType());

        if ($results) {
            return $results;
        }

        return new WP_Error('1002334', 'Error fetching results', $data);
    }

    protected function update($data, $where, $format = array(), $whereFormat = array(), $table = null) {
        $updated = Util::wpdb()->update(
            ($table ?: $this->table),
            $data,
            $where,
            $format,
            $whereFormat
        );

        return $updated;
    }

    protected function insert($data, $format = array(), $table = null) {
        Util::v('===============================================');
        Util::v($this->table);
        Util::p($data);
        Util::v('===============================================');
        $inserted = Util::wpdb()->insert(
            ($table ?: $this->table),
            $data,
            $format
        );

        Util::v('Inserted: ', $inserted);

        Util::p(Util::wpdb());

        if ($inserted) {
            return true;
        }

        return new WP_Error('1002335', 'Error inserting record', array($this->table, $data));
    }

    public function query() {
        $sql = 'SELECT ' . $this->getSelect();
        $sql .= ' FROM ' . $this->getFrom();
        $sql .= ' ' . $this->getJoin();
        $sql .= ($this->getWhere() ? ' WHERE ' . $this->getWhere() : '');
        $sql .= ($this->getOrder() ? ' ORDER BY ' . $this->getOrder() : '');
        $sql .= ($this->getHaving() ? ' HAVING' . $this->getHaving() : '');

        $results = $this->get_results($sql);

        return $results;
    }
}