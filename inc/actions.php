<?php

function alert_wisnet($post_id, $post, $update) {

    // If this is a revision, get real post ID
    if ($post->post_type === 'intent') {
        if ($parent_id = wp_is_post_revision($post_id)) {
            $post_id = $parent_id;
        }

        // Check if this post is an autosave
        if (!wp_is_post_autosave($post_id)) {
            // unhook this function so it doesn't loop infinitely
            remove_action('save_post', 'alert_wisnet');

            // update the post, which calls save_post again
            $data = array(
                'postID' => $post_id,
                'update' => $update,
                'post' => array(
                    'title' => $post->post_title,
                    'content' => $post->post_content,
                    'intent_action_name' => get_field('action_name', $post_id),
                ),
            );

            $jsonData = json_encode($data);
            $IntentRequest = new Ai_Controller_Request_Intent();
            $IntentRequest->addHeader(CURLOPT_POSTFIELDS, $jsonData);
            $IntentRequest->addHeader(CURLOPT_CUSTOMREQUEST, 'PUT');
            $IntentRequest->addHTTPHeader('method', 'PUT');
            $IntentRequest->addHTTPHeader('secretkey', get_option('voice_api_secret_key'));
            $IntentRequest->setId($post_id);

            $request = $IntentRequest->send();
//
            // re-hook this function
            add_action('save_post', 'alert_wisnet');
        }
    }
}

add_action('save_post', 'alert_wisnet', 9, 3);

function vac_trashed_post($post_id) {
    // update the post, which calls save_post again
    $data = array(
        'postID' => $post_id,
        'trash' => true,
        'post' => array(
            'Status' => 5,
        ),
    );

    $jsonData = json_encode($data);
    $IntentRequest = new Ai_Controller_Request_Intent();
    $IntentRequest->addHeader(CURLOPT_POSTFIELDS, $jsonData);
    $IntentRequest->addHeader(CURLOPT_CUSTOMREQUEST, 'PUT');
    $IntentRequest->addHTTPHeader('method', 'PUT');
    $IntentRequest->addHTTPHeader('secretkey', get_option('voice_api_secret_key'));
    $IntentRequest->setId($post_id);

    $request = $IntentRequest->send();
}

add_action('trashed_post', 'vac_trashed_post', 10, 1);
function vac_deleted_post($post_id) {
    // update the post, which calls save_post again
    $data = array(
        'postID' => $post_id,
        'trash' => true,
        'post' => array(
            'Status' => 10,
        ),
    );

    $jsonData = json_encode($data);
    $IntentRequest = new Ai_Controller_Request_Intent();
    $IntentRequest->addHeader(CURLOPT_POSTFIELDS, $jsonData);
    $IntentRequest->addHeader(CURLOPT_CUSTOMREQUEST, 'PUT');
    $IntentRequest->addHTTPHeader('method', 'PUT');
    $IntentRequest->addHTTPHeader('secretkey', get_option('voice_api_secret_key'));
    $IntentRequest->setId($post_id);

    $request = $IntentRequest->send();
}

add_action('deleted_post', 'vac_deleted_post', 10, 1);