<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 12/12/16
 * Time: 9:42 PM
 */

add_shortcode( 'ai-webhook', 'aiWebhook' );

function aiWebhook( $attrs ) {
	
	$settings = shortcode_atts(
		array(), $attrs
	);
	
	ob_start();
	
	get_plugin_part( VOICE_PAGE . '/ai-webhook' );
	
	return ob_get_clean();
}