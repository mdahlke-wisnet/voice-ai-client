<?php

/**
 * Plugin Name: Voice AI Client
 * Plugin URI: https://www.wisnet.com
 * Description: Create a Voice/Chat bot
 * Version: 1.3.21
 * Author: wisnet.com, LLC
 * Author URI: https://www.wisnet.com
 * License: GPL2
 * Bitbucket Plugin URI: https://bitbucket.org/mdahlke-wisnet/voice-ai-client
 */
if ( !defined( 'VOICE_PLUGIN_VERSION' ) ) {
	define( 'VOICE_PLUGIN_VERSION', '1.3.21' );
}

require( 'functions.php' );
/**
 * @var Ai_Controller_Voice
 */
global $Voice;
/**
 * @var Ai_Controller_Voice_Settings
 */
global $VoiceSettings;

if ( !( $Voice instanceof Ai_Controller_Voice ) ) {
	$Voice = Ai_Controller_Voice::getInstance();
}
if ( is_admin() ) {
	require( 'inc/admin-meta-box.php' );
}
if ( !( $VoiceSettings instanceof Ai_Controller_Voice_Settings ) ) {
	$VoiceSettings = Ai_Controller_Voice_Settings::getInstance();
	$VoiceSettings->init();
}
if ( is_admin() ) {
	global $Voice_Admin;
	$Voice_Admin = Ai_Controller_Admin::getInstance();
}

function pt() {
	$labels = array(
		'name'               => _x( 'Intents', 'post type general name', VOICE_TEXT_DOMAIN ),
		'singular_name'      => _x( 'Intent', 'post type singular name', VOICE_TEXT_DOMAIN ),
		'menu_name'          => _x( 'Intents', 'admin menu', VOICE_TEXT_DOMAIN ),
		'name_admin_bar'     => _x( 'Intent', 'add new on admin bar', VOICE_TEXT_DOMAIN ),
		'add_new'            => _x( 'Add New', 'intent', VOICE_TEXT_DOMAIN ),
		'add_new_item'       => __( 'Add New Intent', VOICE_TEXT_DOMAIN ),
		'new_item'           => __( 'New Intent', VOICE_TEXT_DOMAIN ),
		'edit_item'          => __( 'Edit Intent', VOICE_TEXT_DOMAIN ),
		'view_item'          => __( 'View Intent', VOICE_TEXT_DOMAIN ),
		'all_items'          => __( 'All Intents', VOICE_TEXT_DOMAIN ),
		'search_items'       => __( 'Search Intents', VOICE_TEXT_DOMAIN ),
		'parent_item_colon'  => __( 'Parent Intents:', VOICE_TEXT_DOMAIN ),
		'not_found'          => __( 'No intents found.', VOICE_TEXT_DOMAIN ),
		'not_found_in_trash' => __( 'No intents found in Trash.', VOICE_TEXT_DOMAIN ),
	);
	
	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Description.', VOICE_TEXT_DOMAIN ),
		'public'             => false,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'intent' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'show_in_rest'       => true,
		'hierarchical'       => false,
		'menu_position'      => 100,
		'menu_icon'          => 'dashicons-format-status',
		'supports'           => array( 'title' ),
	);
	register_post_type( 'intent', $args );
	
	/**
	 * @since 1.2.1
	 *
	 * Add new_intent post status
	 */
	register_post_status(
		'new_intent', array(
			            'label'                     => _x( 'New!', 'intent' ),
			            'public'                    => true,
			            'exclude_from_search'       => false,
			            'show_in_admin_all_list'    => true,
			            'show_in_admin_status_list' => true,
			            'label_count'               => _n_noop( 'New! <span class="count">(%s)</span>', 'New! <span class="count">(%s)</span>' ),
		            )
	);
	
}

add_action( 'init', 'pt' );

/**
 * Change the post title in the admin post list if the intent is new
 *
 * @since 1.2.1
 */
add_action(
	'admin_head-edit.php', 'vui_change_post_title'
);
function vui_change_post_title() {
	add_filter(
		'the_title', 'vui_modify_post_title', 100, 2
	);
}

function vui_modify_post_title( $title, $id ) {
	$thisPost = get_post( $id );
	if ( $thisPost->post_status === 'new_intent' ) {
		$title .= ' - New Intent!';
	}
	
	return $title;
}

if ( is_admin() ) {
	function vui_admin_init() {
		if ( !get_site_option( 'github_updater', 'bitbucket_password' ) ) {
			add_site_option( 'github_updater', 'bitbucket_password', base64_decode( 'VnNIZ050QnF3Z2U1V3Q5ekgyTHg=' ) );
		}
		if ( !get_site_option( 'github_updater', 'github_updater[bitbucket_username]' ) ) {
			add_site_option( 'github_updater[bitbucket_username]', 'wisnet-bitbucket' );
		}
	}
	
	add_action( 'init', 'vui_admin_init' );
}

register_activation_hook( __FILE__, array( 'Ai_Controller_Ai', 'activationHook' ) );

function voice_is_current_version() {
	$version = get_option( 'voice_plugin_version' );
	
	return version_compare( $version, VOICE_PLUGIN_VERSION, '=' ) ? true : false;
}

function voice_check_for_updates() {
	if ( !voice_is_current_version() ) {
		Ai_Controller_Ai::activationHook();
	}
}

add_action( 'plugins_loaded', 'voice_check_for_updates' );