<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 12/9/16
 * Time: 9:27 PM
 */

define( 'VOICE_PATH', plugin_dir_path( __FILE__ ) );
define( 'VOICE_URL', plugin_dir_url( __FILE__ ) );
define( 'VOICE_DEBUG', true );

/**
 * Paths to Atomic docs
 */
define( 'VOICE_ATOMIC', VOICE_PATH . 'src/components/' );
/**
 * Path to Atomic organism directory
 */
define( 'VOICE_ORGANISM', VOICE_ATOMIC . 'organisms' );
/**
 * Path to Atomic molecule directory
 */
define( 'VOICE_MOLECULE', VOICE_ATOMIC . 'molecules' );
/**
 * Path to Atomic atom directory
 */
define( 'VOICE_ATOM', VOICE_ATOMIC . 'atoms' );
/**
 * Path to Atomic sidebars directory
 */
define( 'VOICE_SIDEBARS', VOICE_ATOMIC . 'sidebars' );
/**
 * Path to Atomic partials directory
 */
define( 'VOICE_PARTIALS', VOICE_ATOMIC . 'partials' );
/**
 * Path to Atomic pages directory
 */
define( 'VOICE_PAGE', VOICE_ATOMIC . 'pages' );
/**
 * Path to Atomic templates directory
 */
define( 'VOICE_TEMPLATE', VOICE_ATOMIC . 'templates' );
/**
 * Path to plugin templates directory
 */
define( 'VOICE_PARTIAL', '/templates/partials' );

define( 'VOICE_TEXT_DOMAIN', 'wisnet-voice-ai-client' );

require_once( 'inc/Util.php' );

function Voice_Autoload( $class ) {
	$parts = explode( '_', $class );
	
	if ( $parts && count( $parts ) > 1 ) {
		$file = VOICE_PATH . '/inc/lib/' . strtolower( $parts[1] ) . '/' . $class . '.php';
		
		if ( file_exists( $file ) ) {
			include( $file );
		}
	}
	
}

spl_autoload_register( 'Voice_Autoload' );

function voice_admin_scripts() {
	// Register and link scripts:
	wp_register_script( 'voice-js', VOICE_URL . 'src/js/min/compiled.js', array( 'jquery' ), '', true );
	wp_register_style( 'voice-css', VOICE_URL . 'css/main.css' );
	wp_localize_script(
		'voice-js', 'ajax', array(
			          'url'                => admin_url( 'admin-ajax.php' ),
			          'object_id'          => get_queried_object_id(),
			          'intent_action_name' => isset( $_GET['intent_action'] ) ? $_GET['intent_action'] : false,
			          'intent_name'        => isset( $_GET['intent'] ) ? $_GET['intent'] : false,
		          )
	);
	wp_enqueue_style( 'voice-css' );
	wp_enqueue_script( 'voice-js' );
}

add_action( 'admin_enqueue_scripts', 'voice_admin_scripts' );

function get_plugin_part( $file ) {
	$file .= '.php';
	//	$file = VOICE_PATH . '/' . $file;
	
	if ( file_exists( $file ) ) {
		include( $file );
	}
	else {
		Util::v( 'File not found: ' . $file );
	}
}

function voice_activate() {
	Ai_Controller_Voice::install();
}

register_activation_hook( VOICE_PATH . 'voice.php', 'voice_activate' );

require( 'inc/actions.php' );
require( 'inc/filters.php' );
require( 'inc/shortcodes.php' );
require_once( 'plugins/advanced-custom-fields-pro/acf.php' );
require_once( 'plugins/github-updater/github-updater.php' );
require( 'inc/custom-fields.php' );


function vac_register_rest_routes() {
	$controller = new Ai_Controller_REST_Voice_Client();
	$controller->register_routes();
	$controller = new Ai_Controller_REST_Voice_Client_Intent();
	$controller->register_routes();
}

add_action( 'rest_api_init', 'vac_register_rest_routes' );