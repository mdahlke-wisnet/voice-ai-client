=== Voice AI ===
Contributors: wisnet.com LLC
Tags: Google Assistant, Google Home, Amazon Echo
Requires at least: 3.0
Tested up to: 4.3.2
Stable tag: 4.3
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.html


== Description ==

Create a Google Assistant App

= Compatibility =

WP >= v3.5

== Installation ==

1. Copy the `voice-ai` folder into your `wp-content/plugins` folder
2. Activate the Voice AI plugin via the plugins admin page

== Changelog ==

= 1.0.0 =
* Initial Release.

== Frequently Asked Questions ==
= What does it do? =
Easily create a Google Assistant app